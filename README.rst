=================================================
Archive.org Census Monthly Identifier Dump Script
=================================================

This script can be put in a crontab to generate a monthly dump of identifiers 
on archive.org. 

The script checks for the existence of an identifier list for the current
month and will abort if an identifier list for the current month already
exists. The gzipped identifier list will then be uploaded to
archive.org under the item named *archiveteam_census_<YEAR>*. If an item for
the current year does not exist at archive.org, it will create one.

The generated identifier list will be named 
*<YEAR>.<MONTH>-ia_identifiers.txt.gz*.

This script requires an archive.org account to run. It currently runs as
*archiveteam_census*.

Setup Instructions
------------------

Prerequisites
~~~~~~~~~~~~~

* python3
* virtualenv
* jq

Install jq
~~~~~~~~~~

  Install jq (https://stedolan.github.io/jq/)
  (You can put the jq binary in the virtualenv venv/bin/ directory.)

Set up the virtualenv and the internetarchive tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Create the working directory.
  This can be wherever you like, but defaults to /opt/at-census/
  If you put it somewhere other than that, you must update the WD variable
  in extract_identifiers.sh

2. Clone this repo in the working directory

3. Run setup.sh
  This will create the virtualenv and install the internetarchive tool
  and it's prerequisites

Configure the internetarchive tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For the user the script will run as, ensure the ~/.config/ia.ini or ~/.ia
configuration exists with the appropriate crendentials:

cd /opt/at-census; source venv/bin/activate; ia configure

Add script to crontab
~~~~~~~~~~~~~~~~~~~~~

Add the extract-identifiers script to the user's crontab

  crontab -e
  
  This will run at 3:14 on the 1st and 16th of each month:

  14 3 1,16 * * /opt/at-census/cron-census-identifiers/extract-identifiers

TO-DO
~~~~~

- use pv to show download progress/size


