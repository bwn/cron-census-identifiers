#!/usr/bin/env bash

# Create virtualenv in venv
virtualenv ../venv
        
# Install `internetarchive` utility
source ../venv/bin/activate; pip install internetarchive;
