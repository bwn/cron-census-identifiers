#!/usr/bin/env bash

# Fail on nonzero exit
set -e
# Fail on pipeline subshell nonzero exit
set -o pipefail

# Change the following to the working directory containing the virtualenv
WD="/opt/at-census/"

# Set this to false to remove generated itemlists
KEEP_OUTPUT=true

# Set this to false to bypass archive.org checks and create a local itemlist
# This will create just create a identifier list (you could probably just
# use ia search all:1 --itemlist)
# Running CHECK=false with KEEP_OUTPUT=false is pointless.
CHECK=true

# Set this to true to upload to ia
UPLOAD=true

YEAR=$(date '+%Y')
MONTH=$(date '+%m')
DAY=$(date '+%d')

CURR_FILENAME="$YEAR.$MONTH-ia_identifiers.txt.gz"
IA_ITEM="archiveteam_census_$YEAR"

RUN=true
NEW_ITEM=false

# Change to WD and activate virtualenv
cd "$WD"
source venv/bin/activate

# Make sure `jq` is available
command -v jq >/dev/null 2>&1 || { echo "jq required but not found in path.";
    echo "Install jq from package manager or https://stedolan.github.io/jq/";
    echo "* You can put the jq binary into the virtualenv";
    exit 1; }

# Don't upload without checking
if [ "$CHECK" == false ]; then
    echo "Check disabled, will not upload to archive.org"
    UPLOAD=false
fi

# Check for current year's item
if [ "$CHECK" == true ]; then
    # (This just checks for a 0 metadata count in IA_ITEM)
    echo "Checking for current year's item: $IA_ITEM"
    METADATA_LENGTH=$(ia metadata "$IA_ITEM" | jq -s '.[] | length')

    if [ "$METADATA_LENGTH" -lt "1" ]; then
        echo "A new item will be created: $IA_ITEM"
        echo "Happy new year! (Or you've done something strange..)"
        NEW_ITEM=true
    fi

    # Check for existing identifier list for current month/year in IA
    echo "Checking for current month's identifier list at archive.org.."
    echo "($CURR_FILENAME)"
    for line in `ia list "$IA_ITEM"`; do
        if [ "$line" == "$CURR_FILENAME" ]; then
            echo "Identifier export for current month and year already exists.."
            exit 0
        fi
    done
fi

echo "Running IA identifier dump for $YEAR/$MONTH.."
ia search all:1 --itemlist | gzip > "$CURR_FILENAME"

if [ "$UPLOAD" == true ]; then
    echo "Uploading to IA.."
    # ia tool was failing with 'broken pipe' issue (related to metadata?
    # when uploading to existing item?)
    if [ "$NEW_ITEM" == true ]; then
        ia upload "$IA_ITEM" "$CURR_FILENAME" --metadata=\"mediatype:data\" \
            --metadata=\""description:$YEAR Archive.org Census Identifiers"\"\
            --metadata=\"creator:Archiveteam\" \
            --metadata=\"licenseurl:http://creativecommons.org/publicdomain/zero/1.0/\"
    else
        ia upload "$IA_ITEM" "$CURR_FILENAME"
    fi
fi

if [ "$KEEP_OUTPUT" == false ]; then
    echo "Removing identifier dump.."
    rm "$CURR_FILENAME"
else
    echo "Keeping identifier dump.."
fi
